import { readFileSync, lstatSync, existsSync } from 'fs';
import { xml2js } from 'xml-js';
import { uuid } from 'uuidv4';

export type ComponentConfig<T> = {
	name: string;
	attributes: T;
	childConfigs?: ComponentConfig<unknown>[];
};

export type GetLoadTemplateData = (
	options?: LoadTemplateDataOptions
) => LoadTemplateData;

export type LoadTemplateData = (path: string) => Promise<ComponentConfig<any>>;

export type LoadTemplateDataOptions = {
	basePath: string;
};

export type TemplateVariables = { [key: string]: string };

const defaultLoadTemplateDataOptions: LoadTemplateDataOptions = {
	basePath: __dirname
};

export const getLoadTemplateData: GetLoadTemplateData = (
	options = defaultLoadTemplateDataOptions
) => async path => {
	const modulePath = `${options.basePath}${path}`;
	const xmlFilePath =
		existsSync(modulePath) && lstatSync(modulePath).isDirectory()
			? `${options.basePath}${path}/index.xml`
			: `${options.basePath}${path}.xml`;

	if (!existsSync(xmlFilePath)) {
		throw `Not found: ${xmlFilePath}`;
	}

	const xml = readFileSync(xmlFilePath, 'utf8');
	const data = await xml2js(xml, {
		compact: false,
		alwaysArray: true,
		ignoreDeclaration: true,
		ignoreInstruction: true,
		ignoreComment: true,
		ignoreCdata: true,
		ignoreDoctype: true,
		ignoreText: true,
		elementsKey: 'childConfigs'
	});

	return (data as any).childConfigs[0];
};

const replaceVariable = (template: string, name: string, value: string) => {
	const re = new RegExp(`{${name}}`, 'g');
	return template.replace(re, value);
};

export const replaceTemplateVariables = (
	templateVariables: TemplateVariables
) => {
	const uuidValue = uuid();
	const replaceFunction = (
		templateData: ComponentConfig<any>
	): ComponentConfig<any> => {
		const newAttributes: { [key: string]: string } = {};
		for (const key in templateData.attributes) {
			let attributeValue = replaceVariable(
				templateData.attributes[key],
				'@uuid',
				uuidValue
			);
			for (const variable in templateVariables) {
				attributeValue = replaceVariable(
					attributeValue,
					variable,
					templateVariables[variable]
				);
			}
			newAttributes[key] = attributeValue;
		}
		return {
			...templateData,
			attributes: newAttributes,
			...(templateData.childConfigs
				? {
						childConfigs: templateData.childConfigs.map(replaceFunction)
				  }
				: {})
		};
	};

	return replaceFunction;
};

export const replaceTemplateElements = (
	loadTemplateData: LoadTemplateData
) => async (
	moduleData: ComponentConfig<any>
): Promise<ComponentConfig<any>> => {
	if (moduleData.childConfigs) {
		const childConfigs = [];
		for (const childConfig of moduleData.childConfigs) {
			if (childConfig.name === 'template') {
				const templateData = await loadTemplateData(
					(childConfig.attributes as any).module
				);

				const templateVariables = Object.keys(
					childConfig.attributes as Record<string, any>
				).reduce((acc, curr) => {
					if (curr.substr(0, 5) === 'data-') {
						const attribute = curr.substr(5);
						return {
							...acc,
							[attribute]: (childConfig.attributes as any)[curr]
						};
					}
					return acc;
				}, {});

				const replacedTemplatedData = replaceTemplateVariables(
					templateVariables
				)(templateData);
				const templateChildConfigs = (
					await replaceTemplateElements(loadTemplateData)(replacedTemplatedData)
				).childConfigs;
				childConfigs.push(...(templateChildConfigs || []));
			} else {
				childConfigs.push(
					await replaceTemplateElements(loadTemplateData)(childConfig)
				);
			}
		}

		return {
			...moduleData,
			childConfigs
		};
	}

	return moduleData;
};

export default {
	getLoadTemplateData,
	replaceTemplateElements
};
