import { uuid } from 'uuidv4';
import {
	replaceTemplateElements,
	LoadTemplateData,
	TemplateVariables,
	replaceTemplateVariables,
	ComponentConfig
} from './template-loader';
jest.mock('uuidv4');

const appendChildConfigs = (
	config: ComponentConfig<any>,
	childConfigs: ComponentConfig<any>[]
) => {
	return {
		...config,
		childConfigs: [...(config.childConfigs || []), ...childConfigs]
	};
};

describe('Template', () => {
	const mainModuleData = {
		name: 'module',
		attributes: {
			label: 'Main module'
		},
		childConfigs: [
			{
				name: 'button',
				attributes: {
					label: 'Test button'
				}
			}
		]
	};

	const templateModuleData = {
		name: 'module',
		attributes: {
			label: 'Template module'
		},
		childConfigs: [
			{
				name: 'box',
				attributes: {
					label: 'Template box'
				}
			},
			{
				name: 'button',
				attributes: {
					label: 'Template button'
				}
			}
		]
	};

	beforeEach(() => {
		jest.clearAllMocks();
	});

	it('should return same moduleData if there are no template elements', async () => {
		const loadTemplateData: LoadTemplateData = () =>
			Promise.resolve({
				name: 'test',
				attributes: {}
			});

		const replacedModuleData = await replaceTemplateElements(loadTemplateData)(
			mainModuleData
		);
		expect(replacedModuleData).toEqual(mainModuleData);
	});

	it('should replace template element with childConfigs of module defined by path in template', async () => {
		const mainModuleWithTemplateData = appendChildConfigs(mainModuleData, [
			{
				name: 'template',
				attributes: {
					module: '//api/module/templateModule'
				}
			}
		]);

		const loadTemplateData: LoadTemplateData = () =>
			Promise.resolve(templateModuleData);

		const expectedModuleData = appendChildConfigs(
			mainModuleData,
			templateModuleData.childConfigs
		);

		const replacedModuleData = await replaceTemplateElements(loadTemplateData)(
			mainModuleWithTemplateData
		);
		expect(replacedModuleData).toEqual(expectedModuleData);
	});

	it('should replace deeply nested template', async () => {
		const mainModuleWithTemplateData = appendChildConfigs(mainModuleData, [
			{
				name: 'box',
				attributes: {
					label: 'Box with template'
				},
				childConfigs: [
					{
						name: 'template',
						attributes: {
							module: '//api/module/templateModule'
						}
					}
				]
			}
		]);

		const loadTemplateData: LoadTemplateData = () =>
			Promise.resolve(templateModuleData);

		const expectedModuleData = appendChildConfigs(mainModuleData, [
			{
				name: 'box',
				attributes: {
					label: 'Box with template'
				},
				childConfigs: templateModuleData.childConfigs
			}
		]);

		const replacedModuleData = await replaceTemplateElements(loadTemplateData)(
			mainModuleWithTemplateData
		);
		expect(replacedModuleData).toEqual(expectedModuleData);
	});

	it('should replace deeply nested template in template', async () => {
		const nestedModulePath = '//api/module/nestedTemplateModule';
		const mainModuleWithTemplateData = appendChildConfigs(mainModuleData, [
			{
				name: 'template',
				attributes: {
					module: nestedModulePath
				}
			},
			{
				name: 'button',
				attributes: {
					label: 'Root button'
				}
			}
		]);

		const nestedTemplateModuleData = {
			name: 'module',
			attributes: {
				label: 'Nested template module'
			},
			childConfigs: [
				{
					name: 'template',
					attributes: {
						module: '//api/module/templateModule'
					}
				},
				{
					name: 'button',
					attributes: {
						label: 'Nested button'
					}
				}
			]
		};

		const loadTemplateData: LoadTemplateData = (path: string) =>
			Promise.resolve(
				path === nestedModulePath
					? nestedTemplateModuleData
					: templateModuleData
			);

		const expectedModuleData = appendChildConfigs(mainModuleData, [
			...templateModuleData.childConfigs,
			{
				name: 'button',
				attributes: {
					label: 'Nested button'
				}
			},
			{
				name: 'button',
				attributes: {
					label: 'Root button'
				}
			}
		]);

		const replacedModuleData = await replaceTemplateElements(loadTemplateData)(
			mainModuleWithTemplateData
		);
		expect(replacedModuleData).toEqual(expectedModuleData);
	});

	it('should replace parameters in template', async () => {
		const mainModuleWithTemplateData = appendChildConfigs(mainModuleData, [
			{
				name: 'template',
				attributes: {
					module: '//api/module/templateModule',
					'data-test1': 'value1',
					'data-test2': 'value2'
				}
			}
		]);

		const templateModuleParamsData = {
			name: 'module',
			attributes: {
				label: 'Template module params'
			},
			childConfigs: [
				{
					name: 'box',
					attributes: {
						label: 'Template {test2}'
					}
				},
				{
					name: 'button',
					attributes: {
						label: 'Template {test1} {test1}'
					}
				}
			]
		};

		const loadTemplateData: LoadTemplateData = () =>
			Promise.resolve(templateModuleParamsData);

		const expectedModuleData = appendChildConfigs(mainModuleData, [
			{
				name: 'box',
				attributes: {
					label: 'Template value2'
				}
			},
			{
				name: 'button',
				attributes: {
					label: 'Template value1 value1'
				}
			}
		]);

		const replacedModuleData = await replaceTemplateElements(loadTemplateData)(
			mainModuleWithTemplateData
		);
		expect(replacedModuleData).toEqual(expectedModuleData);
	});

	it('should replace template variables with values', () => {
		const templateVariables: TemplateVariables = {
			test1: 'value1',
			test2: 'value2'
		};

		const result = replaceTemplateVariables(templateVariables)({
			name: 'module',
			attributes: {
				label: 'Module {test1}'
			},
			childConfigs: [
				{
					name: 'button',
					attributes: {
						label: 'Button {test1}'
					}
				},
				{
					name: 'button',
					attributes: {
						label: 'Button {test2}'
					}
				}
			]
		});

		expect(result).toEqual({
			name: 'module',
			attributes: {
				label: 'Module value1'
			},
			childConfigs: [
				{
					name: 'button',
					attributes: {
						label: 'Button value1'
					}
				},
				{
					name: 'button',
					attributes: {
						label: 'Button value2'
					}
				}
			]
		});
	});
});

it('should replace uuid parameter with generated uuid', () => {
	const templateVariables: TemplateVariables = {};
	const uuidValue = 'xxxx-xxxx';
	const mockedUuid = uuid as jest.Mock<string>;
	mockedUuid.mockImplementation(() => uuidValue);

	const result = replaceTemplateVariables(templateVariables)({
		name: 'module',
		attributes: {
			label: 'Module {@uuid}'
		},
		childConfigs: [
			{
				name: 'button',
				attributes: {
					label: 'Button {@uuid}'
				}
			},
			{
				name: 'button',
				attributes: {
					label: 'Button {@uuid}'
				}
			}
		]
	});

	expect(result).toEqual({
		name: 'module',
		attributes: {
			label: `Module ${uuidValue}`
		},
		childConfigs: [
			{
				name: 'button',
				attributes: {
					label: `Button ${uuidValue}`
				}
			},
			{
				name: 'button',
				attributes: {
					label: `Button ${uuidValue}`
				}
			}
		]
	});
});
